<?php

namespace App\Http\Controllers;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class GitHubApiController extends Controller
{

    private $client;

    public function __construct(Client $client){
        $this->client = $client;
    }

    public function getCredetionals(){
        return [ env('GITHUB_LOGIN'), env('GITHUB_PASSWORD')];
    }

    public function form(){
        return response()->view('form');
    }

    public function find(Request $request){
        $request->validate([
            'username' => 'required'
        ]);

        try{
            $apiResponse = $this->client->request(
                'GET',
                'https://api.github.com/users/' . $request->username,
                [ 'auth'  =>$this->getCredetionals() ]
            );
        }catch (RequestException $e){
            return abort(404);
        }

        $user = json_decode($apiResponse->getBody()->getContents());

        return response()->view('user' , [ 'user' => $user ]);
    }

    public function followers(){
        return response()->view('followers');
    }

    public function apiFollowers($username , Request $request){
        try{
            $apiResponse = $this->client->request(
                'GET',
            'https://api.github.com/users/' . $username . '/followers',
                [
                    'query' => $request->only('page'),
                    'auth'  =>$this->getCredetionals()
                ]
            );
        }catch (RequestException $e){
            return abort(404);
        }

        return response($apiResponse->getBody()->getContents());
    }
}