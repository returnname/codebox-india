@extends('layouts.basic')

@section('content')
    <form action="{{ route('find') }}" method="post">
        @csrf
        <div class="form-group">
            <label>Github username:</label>
            <input type="text" class="form-control" name="username" placeholder="Enter username">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection