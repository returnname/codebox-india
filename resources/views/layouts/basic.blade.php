<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Codebox India</title>
</head>
<body>
    <div id="app" class="flex-center position-ref full-height">
        @yield('content')
    </div>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script type="application/javascript" src="{{asset('js/app.js')}}"></script>
</body>
</html>
