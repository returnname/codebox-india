@extends('layouts.basic')

@section('content')
    <div class="table-user">
        <h1>User info:</h1>
        <table class="table table-dark">
            <thead class="thead-dark ">
            <tr>
                <th scope="col">Image</th>
                <th scope="col">Username</th>
                <th scope="col">Count of followers</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <img src="{{ $user->avatar_url }}" alt="" class="img-responsive img-circle" width="40px">
                </td>
                <td>
                    <b>{{ $user->login }}</b>
                </td>
                <td>
                    <a href="{{ route('followers' , [ 'username'=> $user->login ]) }}">{{ $user->followers }}</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection