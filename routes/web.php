<?php

Route::get('/', 'GitHubApiController@form');
Route::post('/user', 'GitHubApiController@find')->name('find');
Route::get('/user/followers', 'GitHubApiController@followers')->name('followers');
Route::get('/api/user/followers/{username}', 'GitHubApiController@apiFollowers');